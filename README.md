Step to start project

In Terminal:
1. cd infrastructure
2. sudo docker-compose up -d
3. sudo docker-compose ps
4. go to src/main/resources/sql/create_objects.sql and copy all code then paste them in database console

** fill in the table one by one ** department -> bloodline -> character -> wand_type -> blood_line_spread -> character_spread -> wand_type_sprea -> students

In Postman:

curl --location --request GET 'http://localhost:8080/api/spreading/list' \
--data-raw ''

curl --location --request GET 'http://localhost:8080/api/spreading/?fullName=Draco Malfoy' \
--data-raw ''

here we can add students to define departments!
